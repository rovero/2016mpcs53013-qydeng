REGISTER /usr/local/mpcs53013/elephant-bird-core-4.14.jar;
REGISTER /usr/local/mpcs53013/elephant-bird-pig-4.14.jar;
REGISTER /usr/local/mpcs53013/elephant-bird-hadoop-compat-4.14.jar;
REGISTER /usr/local/mpcs53013/libthrift-0.9.0.jar;
REGISTER /home/dengqianyu/ingest/uber-ingest_final_project_crime-0.0.1-SNAPSHOT.jar;
REGISTER /home/dengqianyu/ingest/uber-final_project_area-0.0.1-SNAPSHOT.jar
REGISTER /usr/local/mpcs53013/piggybank.jar;

DEFINE WSThriftBytesToTuple com.twitter.elephantbird.pig.piggybank.ThriftBytesToTuple('edu.uchicago.mpcs53013.crimeSummary.CrimeSummary');
RAW_CRIME = LOAD '/inputs/qydeng/thriftCrime' USING org.apache.pig.piggybank.storage.SequenceFileLoader() as (key:long, value: bytearray);
CRIME_SUMMARY = FOREACH RAW_CRIME GENERATE FLATTEN(WSThriftBytesToTuple(value));

DEFINE WSThriftBytesToTuple com.twitter.elephantbird.pig.piggybank.ThriftBytesToTuple('edu.uchicago.mpcs53013.area.Area');
RAW_AREA = LOAD '/inputs/qydeng/thriftArea' USING org.apache.pig.piggybank.storage.SequenceFileLoader() as (key:long, value: bytearray);
AREA_SUMMARY = FOREACH RAW_AREA GENERATE FLATTEN(WSThriftBytesToTuple(value));

CRIME_AND_AREA_RAW = JOIN CRIME_SUMMARY by CrimeSummary::commArea, AREA_SUMMARY by id;
CRIME_AND_AREA = FOREACH CRIME_AND_AREA_RAW GENERATE year,month,day,time,Area::areaName as area,primaryType, latitude, longitude;

STORE CRIME_AND_AREA into '/inputs/qydeng/crime_and_area' Using PigStorage(',');

CRIME_AND_AREA = LOAD '/inputs/qydeng/crime_and_area' Using PigStorage(',') AS (year:int,month:int,day:int,time:int,area:chararray,primaryType:chararray,latitude:double,longitude:double);

CRIME_AND_AREA_TYPE = FOREACH CRIME_AND_AREA GENERATE year,month,day,time,area,latitude,longitude,primaryType,(primaryType == 'THEFT' ? 1 : 0) AS theft,
(primaryType == 'BATTERY' ? 1 : 0) AS battery,(primaryType == 'CRIM SEXUAL ASSAULT' ? 1:0) AS csassault,
(primaryType == 'KIDNAPPING' ? 1:0) AS kidnap,(primaryType == 'MOTOR VEHICLE THEFT' ? 1:0) AS mvtheft,
(primaryType == 'STALKING' ? 1:0) AS stalk, (primaryType == 'HUMAN TRAFFICKING' ? 1:0) AS htraffick,
(primaryType == 'HOMICIDE' ? 1:0) AS homicide,(primaryType == 'OBSCENITY' ? 1:0) AS obscenity,
(primaryType == 'OFFENSE INVOLVING CHILDREN' ? 1:0) AS offensechild,(primaryType == 'ROBBERY' ? 1:0) as robbery,(primaryType == 'SEX OFFENSE' ? 1:0) AS sexoffense;

CRIME_AND_AREA_BY_TIMEAREA = GROUP CRIME_AND_AREA_TYPE BY (time,area);
SUMMED_CRIME_AND_AREA_BY_TIMEAREA = FOREACH CRIME_AND_AREA_BY_TIMEAREA 
   GENERATE (group.time,group.area) AS time_area,
            SUM($1.theft) AS sum_theft,SUM($1.battery) AS sum_battery,SUM($1.csassault) AS sum_csassault,
	    SUM($1.kidnap) AS sum_kidnap,SUM($1.mvtheft) AS sum_mvtheft,SUM($1.stalk) AS sum_stalk,
	    SUM($1.htraffick) AS sum_htraffick,SUM($1.homicide) AS sum_homicide,SUM($1.obscenity) AS sum_obscenity,
            SUM($1.offensechild) AS sum_offensechild,SUM($1.robbery) AS sum_robb,SUM($1.sexoffense) AS sexoffense;             

STORE SUMMED_CRIME_AND_AREA_BY_TIMEAREA INTO 'hbase://qydeng_crime_by_time_area'
  USING org.apache.pig.backend.hadoop.hbase.HBaseStorage(
	'crime:sum_theft,crime:sum_battery,crime:sum_csassault,crime:sum_kidnap,crime:sum_mvtheft,crime:sum_stalk,crime:sum_htraffick,crime:sum_homicide,crime:sum_obscenity,crime:sum_offensechild,crime:sum_robb,crime:sum_sexoffense');
