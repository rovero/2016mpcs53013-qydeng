Description: The goal of this project is to get statistics of the total occurrences of each type of crime by time and community area.

data: data sets include community area and crime data.

html: crime-report.html for the main page. submit-crime.html for manually update crime data and feed to Kafka.

ingest: final_project_area is to preprocess community area data, and ingest_final_project_crime is to preprocess crime data.

perl: perl files with respect to the html files

Crime.pig: processes community area data and crime data and stores crime by time and area in hbase.

QianyuCrimeTopology: This is the storm topology for updating the crime by time and area data in hbase. In my execute function in updateCrime class, I store the new data in a current_crime table,
                     this is actually for me to debug, and can be commentted in.
                     *It works as expected locally and current_crime table receives new data, but when I run it on cluster, it gives connection refused error.