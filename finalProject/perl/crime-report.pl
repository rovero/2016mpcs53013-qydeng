#!/usr/bin/perl -w
# Creates an html table of flight delays by weather for the given route

# Needed includes
use strict;
use warnings;
use 5.10.0;
use HBase::JSONRest;
use CGI qw/:standard/;

# Read the origin and destination airports as CGI parameters
my $time = param('time');
my $area = param('area');
 
# Define a connection template to access the HBase REST server
# If you are on out cluster, hadoop-m will resolve to our Hadoop master
# node, which is running the HBase REST server
my $hbase = HBase::JSONRest->new(host => "hdp-m.c.mpcs53013-2016.internal:2056");

# This function takes a row and gives you the value of the given column
# E.g., cellValue($row, 'delay:rain_delay') gives the value of the
# rain_delay column in the delay family.
# It uses somewhat tricky perl, so you can treat it as a black box
sub cellValue {
    my $row = $_[0];
    my $field_name = $_[1];
    my $row_cells = ${$row}{'columns'};
    foreach my $cell (@$row_cells) {
	if ($$cell{'name'} eq $field_name) {
	    return $$cell{'value'};
	}
    }
    return 'missing';
}

# Query hbase for the route. For example, if the departure airport is ORD
# and the arrival airport is DEN, the "where" clause of the query will
# require the key to equal ORDDEN
my $records = $hbase->get({
  table => 'qydeng_crime_by_time_area',
  where => {
    key_equals => "(".$time.",".$area.")"
  },
});

# There will only be one record for this route, which will be the
# "zeroth" row returned
#my $row = @$records[0];
my $theft=0;
my $battery=0;
my $csassault=0;
my $kidnap=0;
my $mvtheft=0;
my $stalk=0;
my $htraffick=0;
my $homicide=0;
my $obscenity=0;
my $offensechild=0;
my $robb=0;
my $sexoffense=0;
foreach my $row (@$records){
	if (cellValue($row, 'crime:sum_theft')!='missing'){
		$theft = $theft+cellValue($row, 'crime:sum_theft');	
	}
	if (cellValue($row, 'crime:sum_battery')!='missing'){
		$battery = $battery+cellValue($row, 'crime:sum_battery');	
	}
	if (cellValue($row, 'crime:sum_csassault')!='missing'){
		$csassault = $csassault+cellValue($row, 'crime:sum_csassault');	
	}
	if (cellValue($row, 'crime:sum_kidnap')!='missing'){
		$kidnap = $kidnap+cellValue($row, 'crime:sum_kidnap');	
	}
	if (cellValue($row, 'crime:sum_mvtheft')!='missing'){
		$mvtheft = $mvtheft+cellValue($row, 'crime:sum_mvtheft');	
	}
	if (cellValue($row, 'crime:sum_stalk')!='missing'){
		$stalk = $stalk+cellValue($row, 'crime:sum_stalk');	
	}
	if (cellValue($row, 'crime:sum_htraffick')!='missing'){
		$htraffick = $htraffick+cellValue($row, 'crime:sum_htraffick');	
	}
	if (cellValue($row, 'crime:sum_homicide')!='missing'){
		$homicide = $homicide+cellValue($row, 'crime:sum_homicide');	
	}
	if (cellValue($row, 'crime:sum_obscenity')!='missing'){
		$obscenity = $obscenity+cellValue($row, 'crime:sum_obscenity');	
	}
	if (cellValue($row, 'crime:sum_offensechild')!='missing'){
		$offensechild = $offensechild+cellValue($row, 'crime:sum_offensechild');	
	}
	if (cellValue($row, 'crime:sum_robb')!='missing'){
		$robb = $robb+cellValue($row, 'crime:sum_robb');	
	}
	if (cellValue($row, 'crime:sum_sexoffenset')!='missing'){
		$sexoffense = $sexoffense+cellValue($row, 'crime:sum_sum_sexoffense');	
	}
}
# Get the value of all the columns we need and store them in named variables
# Perl's ability to assign a list of values all at once is very convenient here
#my($theft, $battery, $csassault, $kidnap, $mvtheft, $stalk, $htraffick, $homicide,
#   $obscenity, $offensechild, $robb, $sexoffense)
# =  (cellValue($row, 'crime:sum_theft'), cellValue($row, 'crime:sum_battery'),
#     cellValue($row, 'crime:sum_csassault'), cellValue($row, 'crime:sum_kidnap'),
#     cellValue($row, 'crime:sum_mvtheft'), cellValue($row, 'crime:sum_stalk'),
#     cellValue($row, 'crime:sum_htraffick'), cellValue($row, 'crime:sum_homicide'),
#     cellValue($row, 'crime:sum_obscenity'), cellValue($row, 'crime:sum_offensechild'),
#     cellValue($row, 'crime:sum_robb'), cellValue($row, 'crime:sum_sexoffense'));


# Print an HTML page with the table. Perl CGI has commands for all the
# common HTML tags
print header, start_html(-title=>'hello CGI',-head=>Link({-rel=>'stylesheet',-href=>'/table.css',-type=>'text/css'}));

print     p({-style=>"bottom-margin:10px"});
print table({-class=>'CSS_Table_Example', -style=>'width:60%;margin:auto;'},
	    Tr([td(['Theft', 'Battery', 'Criminal Sexual Assault', 'Kidnapping', 'Motor Vehicle Theft', 'Stalk']),
                td([$theft,$battery,$csassault,$kidnap,$mvtheft, $stalk])])),
    p({-style=>"bottom-margin:10px"})
    ;
print table({-class=>'CSS_Table_Example', -style=>'width:60%;margin:auto;'},
	    Tr([td(['Human Trafficking', 'Homicide', 'Obscenity', 'Offense Involved Child', 'Robbery', 'Sex Offense']),
                td([$htraffick, $homicide, $obscenity, $offensechild, $robb, $sexoffense])])),
    p({-style=>"bottom-margin:10px"})
    ;

print end_html;
