#!/usr/bin/perl -w
# Program: cass_sample.pl
# Note: includes bug fixes for Net::Async::CassandraCQL 0.11 version

use strict;
use warnings;
use 5.10.0;
use FindBin;

use Scalar::Util qw(
        blessed
    );
use Try::Tiny;

use Kafka::Connection;
use Kafka::Producer;

use Data::Dumper;
use CGI qw/:standard/, 'Vars';


my $time = param('time');
if(!$time) {
    exit;
}
my $area = param('area');
if(!$area) {
    exit;
}
my $theft = param('theft') ? 1 : 0;
my $battery = param('battery') ? 1 : 0;
my $csassault = param('csassault') ? 1 : 0;
my $kidnap = param('kidnap') ? 1 : 0;
my $mvtheft = param('mvtheft') ? 1 : 0;
my $stalk = param('stalk ') ? 1 : 0;
my $htraffick = param('$htraffick') ? 1 : 0;
my $homicide = param('homicide') ? 1 : 0;
my $obscenity = param('obscenity') ? 1 : 0;
my $offensechild = param('offensechild') ? 1 : 0;
my $robb = param('robb') ? 1 : 0;
my $sexoffense = param('sexoffense') ? 1 : 0;

my ( $connection, $producer );
try {
    #-- Connection
    # $connection = Kafka::Connection->new( host => 'localhost', port => 6667 );
    $connection = Kafka::Connection->new( host => 'localhost', port => 9092 );

    #-- Producer
    $producer = Kafka::Producer->new( Connection => $connection );
    
    my $message = "<current_observation><time>".param("time")."</time>";
    $message .= "<area>".param("area")."</area><type>";
    if($theft) { $message .= "theft "; }
    if($battery) { $message .= "battery "; }
    if($csassault) { $message .= "csassault "; }
    if($kidnap) { $message .= "kidnap "; }
    if($mvtheft) { $message .= "mvtheft "; }
    if($stalk) { $message .= "stalk "; }
    if($htraffick) { $message .= "htraffick "; }
    if($homicide) { $message .= "homicide "; }
    if($obscenity) { $message .= "obscenity "; }
    if($offensechild) { $message .= "offensechild "; }
    if($robb) { $message .= "robb "; }
    if($sexoffense) { $message .= "sexoffense "; }
    $message .= "</type></current_observation>";

    # Sending a single message
    my $response = $producer->send(
	'qydeng-crime-events',          # topic
	0,                                 # partition
	$message                           # message
        );
} catch {
    if ( blessed( $_ ) && $_->isa( 'Kafka::Exception' ) ) {
	warn 'Error: (', $_->code, ') ',  $_->message, "\n";
	exit;
    } else {
	die $_;
    }
};

# Closes the producer and cleans up
undef $producer;
undef $connection;

print header, start_html(-title=>'Submit crime',-head=>Link({-rel=>'stylesheet',-href=>'/table.css',-type=>'text/css'}));
print table({-class=>'CSS_Table_Example', -style=>'width:80%;'},
            caption('Crime report submitted'),
	    Tr([th(["time","area","Theft","Battery","Criminal Sexual Assault", "Kidnapping","Motor Vehicle Theft","Stalking",
                    "Human Trafficking","Homicide","Obscenity","Offense Involved Child","Robbery","Sex Offense"]),
	        td([$time, $area, $theft, $battery, $csassault, $kidnap, $mvtheft,$stalk,
                    $htraffick, $homicide, $obscenity, $offensechild,$robb,$sexoffense])]));

#print $protocol->getTransport->getBuffer;
print end_html;

