namespace java edu.uchicago.mpcs53013
//nodes
union UserID {
	1: i64 user_id;
}

//edges
struct Friend {
	1: required UserID id1;
        2: required UserID id2;
}
struct InRelationship{
	1: required UserID id1;
	2: required UserID id2;
}
//properties

enum GenderType{
	MALE = 1,
	FEMALE = 2
}
struct Date {
	1: i16 year;
        2: byte month;
	3: byte day;
}
struct Location {
	1: optional string city;
	2: optional string state;
	3: optional string country;
}
//Can we make UserPropertyValue a struct?
union UserPropertyValue{
	1: string full_name;
	2: GenderType gender;
	3: Location location;
	4: Date birthDate;
}
struct UserProperty{
	1: required UserID id;
	2: required UserPropertyValue pval;
}
//tying together
union DataUnit {
	1: UserProperty userprop;
	2: Friend friend;
	3: InRelationship relationship;
}
