package edu.uchicago.mpcs53013;

import java.io.File;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;

public class DemoHadoopThriftSerialization {

	public static void main(String[] args) {
		try {
			Configuration conf = new Configuration();
			conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
			String serializations = conf.get("io.serializations").trim();
			String delim = serializations.isEmpty() ? "" : ",";
			conf.set("io.serializations", serializations + delim + LocationSerialization.class.getName());
			FileSystem fs = FileSystem.get(conf);
			Path seqFilePath = new Path("/tmp/thrift.out");
			SequenceFile.Writer writer = SequenceFile.createWriter(conf, SequenceFile.Writer.file(seqFilePath),
					SequenceFile.Writer.keyClass(Location.class), SequenceFile.Writer.valueClass(IntWritable.class),
					SequenceFile.Writer.compression(CompressionType.NONE));
			
			Location loc = new Location();
			loc.setCity("Los Angeles");
			writer.append(loc,new IntWritable(1));
			loc.setCity("Chicago");
			loc.setState("Illinois");
			loc.setCountry("US");
			writer.append(loc,new IntWritable(2));
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
