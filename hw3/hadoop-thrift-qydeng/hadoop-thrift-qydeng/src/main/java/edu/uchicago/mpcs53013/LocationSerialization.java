package edu.uchicago.mpcs53013;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serialization;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
public class LocationSerialization implements Serialization<Location> {

	class LocationDeserializer implements Deserializer<Location> {
		  private TProtocol protocol;

		@Override
		public void close() throws IOException {
		}

		@Override
		public Location deserialize(Location loc) throws IOException {
			if(loc == null) {
				loc = new Location();
			}
			try {
				loc.read(protocol);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return loc;
		}

		@Override
		public void open(InputStream is) throws IOException {
			protocol = new TJSONProtocol(new TIOStreamTransport(is));
		}
		
	}
	
	class LocationSerializer implements Serializer<Location> {
		  private TProtocol protocol;

		@Override
		public void close() throws IOException {
		}

		@Override
		public void open(OutputStream os) throws IOException {
			protocol = new TJSONProtocol(new TIOStreamTransport(os));
		}

		@Override
		public void serialize(Location loc) throws IOException {
			try {
				loc.write(protocol);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	@Override
	public boolean accept(Class<?> clazz) {
		return clazz.equals(Location.class);
	}

	@Override
	public Deserializer<Location> getDeserializer(Class<Location> clazz) {
		return new LocationDeserializer();
	}

	@Override
	public Serializer<Location> getSerializer(Class<Location> clazz) {
		return new LocationSerializer();
	}

}
