package edu.uchicago.mpcs53013;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;

public class DemoHadoopThriftSerialization {

	public static void main(String[] args) {
		try {
			Configuration conf = new Configuration();
			conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
			String serializations = conf.get("io.serializations").trim();
			String delim = serializations.isEmpty() ? "" : ",";
			conf.set("io.serializations", serializations + delim + WeatherSerialization.class.getName());
			FileSystem fs = FileSystem.get(conf);
			Path seqFilePath = new Path("/tmp/weather.out");
			SequenceFile.Writer writer = SequenceFile.createWriter(conf, SequenceFile.Writer.file(seqFilePath),
					SequenceFile.Writer.keyClass(Weather.class), SequenceFile.Writer.valueClass(IntWritable.class),
					SequenceFile.Writer.compression(CompressionType.NONE));
			FileReader f = new FileReader("/home/mpcs53013/workspace/hadoop-thrift-weather/src/main/java/edu/uchicago/mpcs53013/007026-99999-2016.op");
			BufferedReader reader = new BufferedReader(f);
			String str = reader.readLine();
			int count = 1;
			while((str=reader.readLine())!=null){
				String[] array = str.split("\\s+");
				Weather w = new Weather();
				String stn = array[0];
				w.setStation(stn);
				byte wban = (byte) Integer.parseInt(array[1]);
				w.setWban(wban);
				String ymd = array[2];
				byte year = (byte)Integer.parseInt(ymd.substring(0, 4));
				w.setYear(year);
				byte moda = (byte)Integer.parseInt(ymd.substring(4,ymd.length()));
				w.setModa(moda);
				double temp = Double.parseDouble(array[3]);
				w.setTemp(temp);
				byte cnt_temp = (byte)Integer.parseInt(array[4]);
				w.setCnt_temp(cnt_temp);
				double dewp = Double.parseDouble(array[5]);
				w.setDewp(dewp);
				byte cnt_dewp = (byte)Integer.parseInt(array[6]);
				w.setCnt_dewp(cnt_dewp);
				double slp = Double.parseDouble(array[7]);
				w.setSlp(slp);
				byte cnt_slp = (byte)Integer.parseInt(array[8]);
				w.setCnt_slp(cnt_slp);
				double stp = Double.parseDouble(array[9]);
				w.setStp(stp);
				byte cnt_stp = (byte)Integer.parseInt(array[10]);
				w.setCnt_stp(cnt_stp);
				double visib = Double.parseDouble(array[11]);
				w.setVisib(visib);
				byte cnt_visib = (byte)Integer.parseInt(array[12]);
				w.setCnt_visib(cnt_visib);
				double wdsp = Double.parseDouble(array[13]);
				w.setWdsp(wdsp);
				byte cnt_wdsp = (byte)Integer.parseInt(array[14]);
				double mxsp = Double.parseDouble(array[15]);
				w.setMxspd(mxsp);
				double gust = Double.parseDouble(array[16]);
				w.setGust(gust);
				boolean maxflag = false;
				if(array[17].charAt(array[17].length()-1)=='*'){
					maxflag = true;
				}
				w.setFlag_max(maxflag);
				if(!maxflag){
					w.setMax_temp(Double.parseDouble(array[17]));
				}
				else{
					w.setMax_temp(Double.parseDouble(array[17].substring(0, array[17].length()-1)));
				}
				boolean minflag = false;
				if(array[18].charAt(array[18].length()-1)=='*'){
					minflag = true;
				}
				w.setFlag_min(minflag);
				if(!minflag){
					w.setMin_temp(Double.parseDouble(array[18]));
				}
				else{
					w.setMin_temp(Double.parseDouble(array[18].substring(0, array[18].length()-1)));
				}
				if(array[19].equals(99.9)){
					w.setPrcp(99.9);
				}
				else{
					w.setPrcp(Double.parseDouble(array[19].substring(0, array[19].length()-1)));
					w.setFlag(""+array[18].charAt(array[19].length()-1));
				}
				Double sndp = Double.parseDouble(array[20]);
				w.setSndp(sndp);
				w.setFrshtt(array[21]);
				writer.append(w,new IntWritable(count));
				count++;
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
