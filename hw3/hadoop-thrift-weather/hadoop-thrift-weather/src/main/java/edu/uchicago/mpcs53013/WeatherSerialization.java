package edu.uchicago.mpcs53013;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serialization;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
public class WeatherSerialization implements Serialization<Weather> {

	class WeatherDeserializer implements Deserializer<Weather> {
		  private TProtocol protocol;

		@Override
		public void close() throws IOException {
		}

		@Override
		public Weather deserialize(Weather w) throws IOException {
			if(w == null) {
				w = new Weather();
			}
			try {
				w.read(protocol);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return w;
		}

		@Override
		public void open(InputStream is) throws IOException {
			protocol = new TJSONProtocol(new TIOStreamTransport(is));
		}
		
	}
	
	class WeatherSerializer implements Serializer<Weather> {
		  private TProtocol protocol;

		@Override
		public void close() throws IOException {
		}

		@Override
		public void open(OutputStream os) throws IOException {
			protocol = new TJSONProtocol(new TIOStreamTransport(os));
		}

		@Override
		public void serialize(Weather w) throws IOException {
			try {
				w.write(protocol);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	@Override
	public boolean accept(Class<?> clazz) {
		return clazz.equals(Weather.class);
	}

	@Override
	public Deserializer<Weather> getDeserializer(Class<Weather> clazz) {
		return new WeatherDeserializer();
	}

	@Override
	public Serializer<Weather> getSerializer(Class<Weather> clazz) {
		return new WeatherSerializer();
	}

}
