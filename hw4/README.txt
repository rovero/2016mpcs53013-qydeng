WordCount: this is the original file of wordcount

WordCount_Combiner: this is the wordcount with combiner for hw4-1

WordCount2: this is the program for hw4-2, hw4-2output is an output using
            100.txt as an input

WordCount3: this is the program for extra-credit hw4-3, I use a two-step mapreduce,
            It cannot produce an output, but in the output folder I targetted, I can
            see the _SUCCESS File and an empty part-r-00000. I don't know why this
            happens.  