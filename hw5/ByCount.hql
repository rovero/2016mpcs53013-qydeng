DROP TABLE docs;
CREATE TABLE docs (line STRING);
LOAD DATA INPATH '/tmp/gutenberg/books.txt' OVERWRITE INTO TABLE docs;
DROP TABLE word_counts;
CREATE TABLE word_counts AS
 SELECT word, count(1) AS count FROM 
   (SELECT explode(split(line, '\\s')) AS word FROM docs) w
 GROUP BY word
 ORDER BY word;
DROP TABLE by_count;
CREATE TABLE by_count AS
 SELECT count, count(1) AS wordnum FROM word_counts GROUP BY count ORDER BY count;

