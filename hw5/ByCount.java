
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

public class ByCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        context.write(word, one);
      }
    }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }
  
  public static class ReversingMapper
  	extends Mapper<Text, IntWritable, IntWritable, Text> {
	  public void map(Text key, IntWritable value, Context context
			  ) throws IOException, InterruptedException {
		  context.write(value, key);
	  }
  }

  public static class ByCountReducer
  extends Reducer<IntWritable,Text,IntWritable, IntWritable> {

	  public void reduce(IntWritable key, Iterable<Text> values,
                  Context context
                  ) throws IOException, InterruptedException {
	  int i = 0;
	  for(Text v: values){
		  i++;
	  }
	  context.write(key, new IntWritable(i));
	}
}
  
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(ByCount.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    Path intermediatePath = new Path("/tmp/ByCountIntermediate");
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, intermediatePath);
    if(!job.waitForCompletion(true))
    	System.exit(1);
    job = Job.getInstance(conf, "group by size");
    job.setJarByClass(ByCount.class);
    job.setInputFormatClass(SequenceFileInputFormat.class);
    job.setMapperClass(ReversingMapper.class);
    job.setReducerClass(ByCountReducer.class);
    job.setOutputKeyClass(IntWritable.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, intermediatePath);
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}