A = load '/tmp/gutenberg/books.txt' as line;
B = foreach A generate flatten(TOKENIZE((chararray)$0)) as word;
C = group B by word;
D = foreach C generate COUNT(B) AS occurrences, group as word;
E = group D by occurrences;
F = foreach E generate SIZE(D) as wordnum, group as occurrences;

Dump F;