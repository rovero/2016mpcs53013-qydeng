ADD JAR /home/mpcs53013/workspace/ingest_weather_qydeng/target/uber-ingest_weather_qydeng-0.0.1-SNAPSHOT.jar;

CREATE EXTERNAL TABLE WeatherTable 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.thrift.ThriftDeserializer'
WITH SERDEPROPERTIES ('serialization.class' = 'edu.uchicago.mpcs53013.weatherSummary.WeatherSummary',
                      'serialization.format' = 'org.apache.thrift.protocol.TBinaryProtocol')STORED AS 
SEQUENCEFILE LOCATION '/inputs/thriftWeather';


SELECT month,day,meanTemperature FROM (SELECT month,day,meanTemperature,
rank() over (order by meanTemperature desc)as r FROM WeatherTable) W WHERE W.r = 1;