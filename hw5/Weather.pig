Register /home/mpcs53013/workspace/ingest_weather_qydeng/target/uber-ingest_weather_qydeng-0.0.1-SNAPSHOT.jar;

DEFINE ThriftBytesToTupleDef com.twitter.elephantbird.pig.piggybank.ThriftBytesToTuple('XXXXX');

raw_data = load '/inputs/thriftWeather' using TextLoader() as (record:chararray);
decoded_data = FOREACH raw_data GENERATE ThriftBytesToTupleDef($0);

A = ORDER decoded_data BY meanTemperature DESC;
B = LIMIT A 1;
DUMP B;