REGISTER /usr/local/mpcs53013/elephant-bird-core-4.14.jar;
REGISTER /usr/local/mpcs53013/elephant-bird-pig-4.14.jar;
REGISTER /usr/local/mpcs53013/elephant-bird-hadoop-compat-4.14.jar;
REGISTER /usr/local/mpcs53013/libthrift-0.9.0.jar;
register /home/mpcs53013/workspace/ingest_weather_qydeng/target/uber-ingest_weather_qydeng-0.0.1-SNAPSHOT.jar;
REGISTER /usr/local/mpcs53013/piggybank.jar;

DEFINE WSThriftBytesToTuple com.twitter.elephantbird.pig.piggybank.ThriftBytesToTuple('edu.uchicago.mpcs53013.weatherSummary.WeatherSummary');
FLIGHTS_AND_WEATHER = LOAD '/inputs/flights_and_weather/part*' USING PigStorage(',')
  as (year: int, month:int, day:int, carrier, origin, dep_code, dest, dep_delay, arr_delay, meanTemperature, meanVisibility, meanWindSpeed, fog, rain, snow,
  hail, thunder, tornado);

WEATHER_FLIGHTS = FOREACH FLIGHTS_AND_WEATHER GENERATE origin,dest,dep_delay,fog,rain,snow,hail,thunder,tornado;

CLEAR_FLIGHTS = FILTER WEATHER_FLIGHTS BY fog==0 AND rain==0 AND snow==0 AND hail==0 AND thunder==0 AND tornado==0;
CLEAR_FLIGHTS_BY_ROUTE = GROUP CLEAR_FLIGHTS BY (origin, dest);
CLEAR_DELAY_BY_ROUTE = FOREACH CLEAR_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

FOG_FLIGHTS = FILTER WEATHER_FLIGHTS BY fog==1;
FOG_FLIGHTS_BY_ROUTE = GROUP FOG_FLIGHTS BY (origin, dest);
FOG_DELAY_BY_ROUTE = FOREACH FOG_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

RAIN_FLIGHTS = FILTER WEATHER_FLIGHTS BY rain==1;
RAIN_FLIGHTS_BY_ROUTE = GROUP RAIN_FLIGHTS BY (origin, dest);
RAIN_DELAY_BY_ROUTE = FOREACH RAIN_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

SNOW_FLIGHTS = FILTER WEATHER_FLIGHTS BY snow==1;
SNOW_FLIGHTS_BY_ROUTE = GROUP SNOW_FLIGHTS BY (origin, dest);
SNOW_DELAY_BY_ROUTE = FOREACH SNOW_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

HAIL_FLIGHTS = FILTER WEATHER_FLIGHTS BY hail==1;
HAIL_FLIGHTS_BY_ROUTE = GROUP HAIL_FLIGHTS BY (origin, dest);
HAIL_DELAY_BY_ROUTE = FOREACH HAIL_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

THUNDER_FLIGHTS = FILTER WEATHER_FLIGHTS BY thunder==1;
THUNDER_FLIGHTS_BY_ROUTE = GROUP THUNDER_FLIGHTS BY (origin, dest);
THUNDER_DELAY_BY_ROUTE = FOREACH THUNDER_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

TORNADO_FLIGHTS = FILTER WEATHER_FLIGHTS BY tornado==1;
TORNADO_FLIGHTS_BY_ROUTE = GROUP TORNADO_FLIGHTS BY (origin, dest);
TORNADO_DELAY_BY_ROUTE = FOREACH TORNADO_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;

//hw6-2:suppose we have joined new flight data and new weather data to get new _flights_and_weather

NEW_FLIGHTS_AND_WEATHER = LOAD '/inputs/new_flights_and_weather' USING PigStorage(',')
  as (year: int, month:int, day:int, carrier, origin, dep_code, dest, dep_delay, arr_delay, meanTemperature, meanVisibility, meanWindSpeed, fog, rain, snow,
  hail, thunder, tornado);

NEW_WEATHER_FLIGHTS = FOREACH NEW_FLIGHTS_AND_WEATHER GENERATE origin,dest,dep_delay,fog,rain,snow,hail,thunder,tornado;

NEW_CLEAR_FLIGHTS = FILTER NEW_WEATHER_FLIGHTS BY fog==0 AND rain==0 AND snow==0 AND hail==0 AND thunder==0 AND tornado==0;
NEW_CLEAR_FLIGHTS_BY_ROUTE = GROUP NEW_CLEAR_FLIGHTS BY (origin, dest);
NEW_CLEAR_DELAY_BY_ROUTE = FOREACH NEW_CLEAR_FLIGHTS_BY_ROUTE GENERATE group AS route, AVG($1.dep_delay) AS dep_delay;
CLEAR_DELAY_BY_ROUTE_COMBINE = JOIN NEW_CLEAR_DELAY_BY_ROUTE by route, CLEAR_DELAY_BY_ROUTE by route;
CLEAR_DELAY_by_ROUTE = FOREACH CLEAR_DELAY_BY_ROUTE_COMBINE GENERATE NEW_CLEAR_DELAY_BY_ROUTE::route AS route,($1+$3)/2 as dep_delay;





